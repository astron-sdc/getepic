# getepic

Tool and library to obtain Persistent Identifiers (PIDs) from the EPIC PID registry using the REST API from the registry service. 

The minimum requirement to run this tool is python *3.7*. 

## Package contents
The package consists of three libraries. The `pid` library contains a client that can be used to request, modify and even delete EPIC PIDs. In the module `piddb`, the methods are fount that can be used to insert the acquired PID in a database. Finally the `epic_consts` module defines three lists with object types, error codes and formats to be used for EPIc PIDs. 


## Usage
*NOTE*: The install requirements do not add connectors to databases. For instance, if the configuration of the database server connects to a postgres server (the default SDCO configuration), you will need to eplicitly install `psycopg2`:

```
pip3 install psycopg2-binary
```
A handle consists of entries that each have an index. Even though multiple indices could be coupled to a single handle, in general our handles simply consist of an admin entry (defining who the admin is; with index 100) and an entry defining what the handle should link to (generally with index 1).

The [Handle tool documentation](https://www.handle.net/tech_manual/HandleTool_Ver2.pdf) contains more information on the details of the handle definitions. Especially the list of types is worked out in this doc. In the vast majority of the cases the type you want to pick is `URL`. For completeness the following table shows the current set of possible handle types:
| type | description |
|:-------------:|:------------:|
| HS_ADMIN |Defines the administrator. In general you will not want to touch the default value of this entry. |
| URL | A URL to a web page. |
| EMAIL | An email address. |
| HS_ALIAS | Another handle for which this is an alias. |
| HS_SITE | Used to add the site information for prefix handles to indicate where handles with that prefix are resolved. |
| HS_VLIST | Administrator groups with a list of other handle values. |
| HS_SECKEY |A secret key. Typically, the `public read permission` for this type of value would be turned off. |
|HS_PUBKEY | A public key. |
| HS_SERV| Site information by specifying a handle the handle record for which has `HS_SITE` values. |
| DESC|UTF8-encoded text descriptions of the object identified by the handle.|

You can install this package using pip:
```
pip3 install git+https://git.astron.nl/astron-sdc/getepic.git
```

The package comes with an executable `getepic` which can be used to request PIDs and add them to the database. The tool can be invoked in several ways:

```
getepic -t URL -l https://www.example.com -c examplecollection config.json
```
This will create one PID, that links to a URL, which is `https://.www.example.com`. The data collection this PID belongs to is called `examplecollection` and the configuration is in the file `config.json` (see below for the format).

For bulk operations, one can create a file in either JSON or csv format. A JSON could look like this
```
[
    {"type":"URL", "target":"https://www.example.com"},
    {"type":"HS_ALIAS", "target":"21.T12995/fd5b511b-15ec-4789-a99d-efaf476f4b65"},
    {"type":"EMAIL", "target":"nobody@astron.nl"}
]
```
This file will define the request of three handles. The first is of type `URL` with target `https://www.example.com`, the second one is an alias to another PID (`HS_ALIAS`), which could be of any type but in this case is another EPIC PID (21.T12995/fd5b511b-15ec-4789-a99d-efaf476f4b65). The third entry generates a handle for an email address (`EMAIL`) which points to `nobody@astron.nl`. 

In csv format, the file would look like this:
```
type,target
URL,target":https://www.example.com
HS_ALIAS,21.T12995/fd5b511b-15ec-4789-a99d-efaf476f4b65
EMAIL,nobody@astron.nl
```
If the CSV or JSON is stored in a file called `handles.csv`, the execution of the command then becomes:
```
getepic -b handles.csv -c examplecollection config.json
```
Alternatively if you want to generate many handles pointing to the same type you can also provide a list of targets as input batch file:
```
https://www.example.com
https://www.example.com/2
https://www.example.com/yet/another/one
```
Since all of those are URLs, we can then use the command as follows (assuming the file is called `urls.dat`):

```
getepic -b urls.dat -l URL -c examplecollection config.json
```

## Configuration
The configuration can be done using a JSON configuration file. An example of this configuration (for the EPIC PID test prefix 12995) is:
```
 {
     "handle_server_url": "https://epic-pid.storage.surfsara.nl",
     "handle_server_port" : "8003",
     "prefix": "21.T12995",
     "private_key": "/home/user/epicpids/21.T12995_USER01_350_privkey.pem",
     "certificate_only": "/home/user/epicpids/21.T12995_USER01_350_certificate_only.pem",
     "database_address": "sqlite:///testd.db"
 }
```
The handle server, port and prefix are to be provided by the organisation that grants the handles. The private key and certificate are used for authentication. However when performing bulk operation one is adviced to use sessions. The `database_address` should be parsable by SQLAlchemy and can point to any database type supported by it. 

 ## Reference documents
On (https://hdl.handle.net) you can parse the handles (actually all handles parse in eachothers parser so you could also try (https://www.doi.org))

[Technical documentation of the EPIC PID service](https://www.handle.net/tech_manual/HN_Tech_Manual_9.pdf).

[SURFsara documentation for the EPIC PId service](https://userinfo.surfsara.nl/systems/epic-pid).
