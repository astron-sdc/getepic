# SPDX-License-Identifier: Apache-2.0
"""Module for setting up and filling a database of EPIC PIDs.
The module variable is a housekeeping variable. The class
is an objevt that uses SQLAlchemy to manage a database of PIDs.
"""
import logging
from sqlalchemy import (
    create_engine,
    MetaData,
    Table,
    Column,
    Integer,
    String,
    ForeignKey,
)
from sqlalchemy.exc import NoResultFound
from getepic.epic_consts import constrained_types

logger = logging.getLogger(__name__)


class PidDB:
    """Client to create a database for EPIC PIDs. To be used in combination with
    tooling to request those PIDs. Each PID (gathered in the pids table) is
    related to a data collection (gathered in the collections table).
    The linktypes table contains all possible link types for EPIC PIDs which
    can be found in the documentation:
    https://www.handle.net/tech_manual/HN_Tech_Manual_9.pdf
    """

    def __init__(self, database):
        """Setup the database (if it does not already exist) by generating the tables
        and fill the linktypes table with the possible link types.

        Parameters
        ----------
        database : str
            Database specification in the SQLAlchemy format. For example a sqlite database
            can be written as "sqlite:///pids.db" or "mysql://user:password@mysqlhost/database"
        """
        self.engine = create_engine(database, echo=False)
        self.meta = MetaData()
        logger.debug("Defining tables...")
        self.pids = Table(
            "epic_pids",
            self.meta,
            Column("id", Integer, primary_key=True),
            Column("epic_pid", String, unique=True),
            Column("linktype", Integer, ForeignKey("linktypes.id")),
            Column("target", String),
            Column("collection", Integer, ForeignKey("collections.id")),
            Column("description", String),
        )
        self.collections = Table(
            "collections",
            self.meta,
            Column("id", Integer, primary_key=True),
            Column("name", String, unique=True),
        )
        self.types = Table(
            "linktypes",
            self.meta,
            Column("id", Integer, primary_key=True),
            Column("linktype", String, unique=True),
        )
        logger.debug("Tables have been defined. Now calling create function...")
        self.meta.create_all(self.engine)
        logger.debug("Tables created. Now filling the types row...")
        self._filltypes()

    def _filltypes(self):
        """Fill the table with constrained types so that validation can happen
        at the database level.
        """
        logger.debug("Called type filler.")
        with self.engine.connect() as conn:
            if len(conn.execute(self.types.select()).all()) != 0:
                logger.debug(
                    "The types table is not "
                    "empty. Assuming we connect to an existing DB"
                )
                return
        insertdict = [{"linktype": insval} for insval in constrained_types]
        insert_stmt = self.types.insert()
        logger.debug(
            f"Inserting with statement {insert_stmt} " "and parameters {insertdict}..."
        )
        with self.engine.connect() as conn:
            conn.execute(insert_stmt, insertdict)

    def insert(self, epic_pid, linktype, target, collection, description=None):
        """Insert EPIC PID in the database based on its properties.

        Parameters
        ----------
        epic_pid : str
            The EPIC PID to insert in the database
        linktype : str
            The type of the link (e.g. URL or HS_ALIAS)
        target : str
            Target location that the PID refers to (e.g. a URL or another PID)
        collection : str
            The data collection the EPIC PID belongs to
        description: str
            Description of the link. This can be particularly useful for aliases.

        Raises
        ------
        ValueError
            If the linktype is not in the standard list
            of linktypes, this exception will be raised.
        """
        logger.debug(
            f"Inserting PID entry with PID {epic_pid} of "
            "type {linktype} to {target} for collection {collection}..."
        )
        link_id_st = self.types.select().with_only_columns([self.types.c.id])
        link_id_st = link_id_st.where(self.types.c.linktype == linktype)
        logger.debug(f"Query type ID using statement {link_id_st}")
        with self.engine.connect() as conn:
            try:
                link_id = conn.execute(link_id_st).one()[0]
            except NoResultFound as nrf:
                logger.error(f"Could not find {linktype} in allowed list of types.")
                raise ValueError(
                    f"type {linktype} not supported as EPIC PID link type."
                ) from nrf
        logger.debug(f"Id of link type {linktype} in database table is {link_id}.")
        coll_id_st = self.collections.select().with_only_columns(
            [self.collections.c.id]
        )
        coll_id_st = coll_id_st.where(self.collections.c.name == collection)
        logger.debug(f"Finding ID of the collection with statement {coll_id_st}.")
        with self.engine.connect() as conn:
            try:
                coll_id = conn.execute(coll_id_st).one()[0]
            except NoResultFound:
                logger.debug(
                    f"Collection {collection} does not exist. "
                    "Adding it to the database."
                )
                conn.execute(self.collections.insert(), {"name": collection})
                coll_id = conn.execute(coll_id_st).one()[0]
                logger.debug(f"Collection created with ID {coll_id}")
            else:
                logger.debug(f"Collection ID in database is {coll_id}")
            pid_data_package = {
                "epic_pid": epic_pid,
                "linktype": link_id,
                "target": target,
                "collection": coll_id,
                "description": description,
            }
            logger.debug(f"Inserting the dict {pid_data_package} in the PID table.")
            conn.execute(
                self.pids.insert(),
                pid_data_package,
            )
