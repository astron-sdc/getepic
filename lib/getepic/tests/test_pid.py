# SPDX-License-Identifier: Apache-2.0
"""Tests for the pid.py class"""
# pylint: disable=W0212 disable=R0904
import unittest
from unittest.mock import patch, mock_open, MagicMock
import json
from getepic import pid
from requests.exceptions import HTTPError
from getepic.pid import HandleClientException

JSON_DATA = """
 {
     "handle_server_url": "https://myhandle.provider.ext",
     "handle_server_port" : "1982",
     "prefix": "31.74883",
     "private_key": "/myprivate.key",
     "certificate_only": "/mycertifi.cate"
 }
"""


class PidTest(unittest.TestCase):
    """Tests for the pid.py class"""

    @staticmethod
    def set_mock_json(mock_set, value):
        """Set json return value for a mock, and reset the mock call afterwards.

        Parameters
        ----------
        mock_set : mock
            The mock object to set the return value for.
        value : str
            The actual content of the json return value.
        """
        mock_set().json.return_value = json.loads(value)
        mock_set.reset_mock()

    def setUp(self):
        with patch("builtins.open", mock_open(read_data=JSON_DATA)):
            self.testclass = pid.HandleClient("demo.json")
        self.logger = patch("getepic.pid.pid.logger").start()

    @patch("getepic.pid.pid.post")
    @patch("getepic.pid.pid.put")
    def test_session_start_ok(self, putmock, postmock):
        """Test if session is correctly obtained, authenticated and added to the object"""
        sessid = "12345"
        post_response = MagicMock()
        post_response.json.return_value = json.loads(f'{{"sessionId":"{sessid}"}}')
        postmock.return_value = post_response
        put_response = MagicMock()
        put_response.json.return_value = json.loads(
            '{"responseCode":"1", "authenticated":true}'
        )
        put_response.raise_for_status.return_value = None
        putmock.return_value = put_response
        self.testclass.start_session()
        self.assertEqual(self.testclass.session, sessid)
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/sessions/this",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": 'Handle clientCert=true, Handle sessionId="12345"',
            },
        )
        postmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/sessions"
        )
        self.testclass.start_session()
        postmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/sessions"
        )
        self.logger.info.assert_called_once()

    @patch("getepic.pid.pid.post")
    @patch("getepic.pid.pid.put")
    def test_session_start_fail_notauth(self, putmock, postmock):
        """Test that session starting raises an exception if the session did not authenticate"""
        sessid = "12345"
        post_response = MagicMock()
        post_response.json.return_value = json.loads(f'{{"sessionId":"{sessid}"}}')
        postmock.return_value = post_response
        put_response = MagicMock()
        put_response.raise_for_status.return_value = None
        putmock.return_value = put_response
        put_response.json.return_value = json.loads(
            '{"responseCode":"1", "authenticate":"false"}'
        )
        self.assertRaises(HandleClientException, self.testclass.start_session)
        self.logger.error.assert_called_once()
        self.logger.error.reset_mock()
        put_response.json.return_value = json.loads('{"responseCode":"1"}')
        self.assertRaises(HandleClientException, self.testclass.start_session)
        self.logger.error.assert_called_once()
        self.logger.error.reset_mock()
        put_response.json.return_value = json.loads(
            '{"responseCode":"1", "authenticated":"False"}'
        )
        self.assertRaises(HandleClientException, self.testclass.start_session)
        self.logger.error.assert_called_once()
        self.logger.error.reset_mock()

        put_response.json.return_value = json.loads('{"responseCode":"101"}')
        self.assertRaises(HandleClientException, self.testclass.start_session)
        self.logger.error.assert_called_once()
        put_response.json.return_value = json.loads('{"responseCode":"1"}')
        put_response.raise_for_status.side_effect = HTTPError("Error error error!")
        self.assertRaises(HTTPError, self.testclass.start_session)
        self.logger.error.assert_called_once()

    @patch("getepic.pid.pid.post")
    def test_requests_post_ok(self, postmock):
        """Test requests wrapper works ok for POST"""
        self.testclass._request(postmock, "fakeurl://fake.fake")
        postmock.assert_called_once_with("fakeurl://fake.fake")

    @patch("getepic.pid.pid.post")
    def test_requests_post_fail(self, postmock):
        """Test requests wrapper fails if POST returns an HTTP error"""
        postmock().raise_for_status.side_effect = HTTPError("Error error error!")
        self.assertRaises(
            HTTPError, self.testclass._request, *(postmock, "fakeurl://fake.fake")
        )

    @patch("getepic.pid.pid.put")
    @patch("getepic.pid.pid.get")
    @patch("getepic.pid.pid.delete")
    def test_requests_pgd_crt_ok(self, delmock, getmock, putmock):
        """Test requests wrapper works for PUT, GET and DELETE authenticating with certificates"""
        self.set_mock_json(putmock, '{"responseCode":"1"}')
        self.set_mock_json(getmock, '{"responseCode":"1"}')
        self.set_mock_json(delmock, '{"responseCode":"1"}')
        self.testclass._request(delmock, "fakeurl://fake.fake")
        delmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )
        self.testclass._request(getmock, "fakeurl://fake.fake")
        getmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )
        self.testclass._request(putmock, "fakeurl://fake.fake")
        delmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )
        # Data only used for put so we only test that.
        putmock.reset_mock()
        self.testclass._request(putmock, "fakeurl://fake.fake", data="{'nice':'json}")
        putmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data="{'nice':'json}",
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.put")
    @patch("getepic.pid.pid.get")
    @patch("getepic.pid.pid.delete")
    def test_requests_pgd_sess_ok(self, delmock, getmock, putmock):
        """Test requests wrapper works for PUT, GET and DELETE in different ways, with sessions"""
        self.set_mock_json(putmock, '{"responseCode":"1"}')
        self.set_mock_json(getmock, '{"NoResponse":"1"}')
        delmock().json.side_effect = json.JSONDecodeError("aaaa", "test\n", 2)
        delmock.reset_mock()
        self.testclass.session = "1234"
        self.testclass._request(delmock, "fakeurl://fake.fake")
        delmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data=None,
            cert=None,
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": 'Handle sessionId="1234"',
            },
        )
        self.testclass._request(getmock, "fakeurl://fake.fake")
        getmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data=None,
            cert=None,
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": 'Handle sessionId="1234"',
            },
        )
        self.testclass._request(putmock, "fakeurl://fake.fake")
        putmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data=None,
            cert=None,
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": 'Handle sessionId="1234"',
            },
        )

        # Data only used for put so we only test that.
        putmock.reset_mock()
        self.testclass._request(putmock, "fakeurl://fake.fake", data="{'nice':'json}")
        putmock.assert_called_once_with(
            "fakeurl://fake.fake",
            data="{'nice':'json}",
            cert=None,
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": 'Handle sessionId="1234"',
            },
        )

    @patch("getepic.pid.pid.put")
    @patch("getepic.pid.pid.get")
    @patch("getepic.pid.delete")
    def test_requests_pgd_http_fail(self, delmock, getmock, putmock):
        """Test requests wrapper fails on HTTP error for PUT, GET and DELETE"""
        self.set_mock_json(putmock, '{"responseCode":"1"}')
        self.set_mock_json(getmock, '{"responseCode":"1"}')
        self.set_mock_json(delmock, '{"responseCode":"1"}')
        delmock().raise_for_status.side_effect = HTTPError("Error error error!")
        self.assertRaises(
            HTTPError, self.testclass._request, *(delmock, "fakeurl://fake.fake")
        )
        getmock().raise_for_status.side_effect = HTTPError("Error error error!")
        self.assertRaises(
            HTTPError, self.testclass._request, *(getmock, "fakeurl://fake.fake")
        )
        putmock().raise_for_status.side_effect = HTTPError("Error error error!")
        self.assertRaises(
            HTTPError, self.testclass._request, *(putmock, "fakeurl://fake.fake")
        )

    @patch("getepic.pid.pid.put")
    @patch("getepic.pid.pid.get")
    @patch("getepic.pid.delete")
    def test_requests_pgd_resp_fail(self, delmock, getmock, putmock):
        """Test requests wrapper fails on respponsecode for PUT, GET and DELETE"""
        self.testclass.delete_session = MagicMock()
        self.set_mock_json(putmock, '{"responseCode":"401"}')
        self.set_mock_json(getmock, '{"responseCode":"202"}')
        self.set_mock_json(delmock, '{"responseCode":"302"}')
        self.testclass.session = "1234"
        self.assertRaises(
            pid.HandleClientException,
            self.testclass._request,
            *(delmock, "fakeurl://fake.fake"),
        )
        self.logger.error.assert_called_once()
        self.logger.error.reset_mock()
        self.assertRaises(
            pid.HandleClientException,
            self.testclass._request,
            *(getmock, "fakeurl://fake.fake"),
        )
        self.logger.error.assert_called_once()
        self.logger.error.reset_mock()
        self.assertRaises(
            pid.HandleClientException,
            self.testclass._request,
            *(putmock, "fakeurl://fake.fake"),
        )
        self.logger.error.assert_called_once()
        self.logger.error.reset_mock()

    def test_reset_handledata(self):
        """Test if handle data is correctly reset"""
        self.testclass.indices = [
            {
                "index": 1,
                "type": "URL",
                "data": {"format": "string", "value": "fakeurl://fake,fake"},
            }
        ]
        self.testclass.handledata = {"Thisfailed": True}
        self.testclass.reset_handledata()
        self.assertFalse(len(self.testclass.indices))
        self.assertDictEqual(
            self.testclass.handledata, {"values": self.testclass.indices}
        )

    def test_add_hdl_entry_ok(self):
        """Test that a handle entry is correctly parsed and data generated"""
        self.testclass.add_hdl_entry(
            "myfakesite", index=3, valtype="URL", dataformat="string"
        )
        self.testclass.add_hdl_entry(
            "myalias", index=5, valtype="HS_ALIAS", dataformat="string"
        )
        compare_dict = {
            "values": [
                {
                    "index": 3,
                    "type": "URL",
                    "data": {"format": "string", "value": "myfakesite"},
                },
                {
                    "index": 5,
                    "type": "HS_ALIAS",
                    "data": {"format": "string", "value": "myalias"},
                },
            ]
        }
        self.assertDictEqual(self.testclass.handledata, compare_dict)

    def test_add_hdl_entry_constraint_fail(self):
        """Test that a handle entry creation fails if constraints are not met"""
        self.assertRaises(
            pid.HandleClientException,
            self.testclass.add_hdl_entry,
            **{"datavalue": "test", "valtype": "FAKE"},
        )
        self.assertRaises(
            pid.HandleClientException,
            self.testclass.add_hdl_entry,
            **{"datavalue": "test", "dataformat": "FAKE"},
        )

    def test_add_hdl_entry_overwrite(self):
        """Test that a handle entry can only be overwritten if flag is set"""
        self.testclass.add_hdl_entry("thisisatest")
        self.assertEqual(
            self.testclass.indices,
            [
                {
                    "index": 1,
                    "type": "URL",
                    "data": {"format": "string", "value": "thisisatest"},
                }
            ],
        )
        self.assertRaises(
            pid.HandleClientException,
            self.testclass.add_hdl_entry,
            **{"datavalue": "overwriter"},
        )
        self.testclass.add_hdl_entry(datavalue="overwritten", overwrite=True)
        self.logger.warning.assert_called_once()
        self.assertEqual(
            self.testclass.indices,
            [
                {
                    "index": 1,
                    "type": "URL",
                    "data": {"format": "string", "value": "overwritten"},
                }
            ],
        )

    @patch("getepic.pid.pid.uuid.uuid4")
    @patch("getepic.pid.pid.put")
    def test_request_handle_ok(self, putmock, uuidmock):
        """Test that requesting a handle generates the correct URL and data"""
        self.set_mock_json(putmock, '{"responseCode":"1", "handle":"31.74883/newpid"}')
        uuidmock.return_value = "newpid"
        handle1 = "thand1"
        handle5 = "thand2"
        htype1 = "URL"
        htype5 = "HS_ALIAS"
        self.testclass.add_hdl_entry(handle1, valtype=htype1)
        self.testclass.add_hdl_entry(handle5, index=5, valtype=htype5)
        self.testclass.request_handle()
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/newpid",
            data='{"values": [{"index": 1, "type": "URL", '
            '"data": {"format": "string", "value": "thand1"}},'
            ' {"index": 5, "type": "HS_ALIAS", "data": {"format": '
            '"string", "value": "thand2"}}, {"index": 100, '
            '"type": "HS_ADMIN", "data": {"format": "admin", '
            '"value": {"handle": "0.NA/31.74883", "index": 200, '
            '"permissions": "011111110011"}}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )
        self.testclass.request_handle(suffix=None)
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/newpid",
            data='{"values": [{"index": 1, "type": "URL", '
            '"data": {"format": "string", "value": "thand1"}},'
            ' {"index": 5, "type": "HS_ALIAS", "data": {"format": '
            '"string", "value": "thand2"}}, {"index": 100, '
            '"type": "HS_ADMIN", "data": {"format": "admin", '
            '"value": {"handle": "0.NA/31.74883", "index": 200, '
            '"permissions": "011111110011"}}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )


    
    @patch("getepic.pid.pid.put")
    def test_request_handle_ok(self, putmock):
        """Test that requesting a handle with manual suffix generates the right URL"""
        handle1 = "thand1"
        handle5 = "thand2"
        htype1 = "URL"
        htype5 = "HS_ALIAS"
        self.testclass.add_hdl_entry(handle1, valtype=htype1)
        self.testclass.add_hdl_entry(handle5, index=5, valtype=htype5)
        self.testclass.request_handle(suffix="supersuffix")
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/supersuffix",
            data='{"values": [{"index": 1, "type": "URL", '
            '"data": {"format": "string", "value": "thand1"}},'
            ' {"index": 5, "type": "HS_ALIAS", "data": {"format": '
            '"string", "value": "thand2"}}, {"index": 100, '
            '"type": "HS_ADMIN", "data": {"format": "admin", '
            '"value": {"handle": "0.NA/31.74883", "index": 200, '
            '"permissions": "011111110011"}}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    def test_request_handle_nofirstid(self):
        """Test that requesting a handle fails if no id=1 entry"""
        handle5 = "thand2"
        htype5 = "HS_ALIAS"
        self.testclass.add_hdl_entry(handle5, index=5, valtype=htype5)
        self.assertRaises(pid.HandleClientException, self.testclass.request_handle)

    @patch("getepic.pid.pid.uuid.uuid4")
    @patch("getepic.pid.pid.put")
    def test_simple_url_ok(self, putmock, uuidmock):
        """Test that a simple url-based handle is requested correctly (right URL and data)"""
        self.set_mock_json(putmock, '{"responseCode":"1", "handle":"31.74883/newpid"}')
        uuidmock.return_value = "newpid"
        self.testclass.simple_url("fake://fakeurl.fake")
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/newpid",
            data='{"values": [{"index": 1, "type": "URL", '
            '"data": {"format": "string", "value": "fake://fakeurl.fake"}}, '
            '{"index": 100, "type": "HS_ADMIN", "data": {"format": "admin", '
            '"value": {"handle": "0.NA/31.74883", "index": 200, '
            '"permissions": "011111110011"}}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.uuid.uuid4")
    @patch("getepic.pid.pid.put")
    @patch("getepic.pid.pid.get")
    def test_simple_alias_ok(self, getmock, putmock, uuidmock):
        """Test that a simple alias-based handle is requested correctly (right URL and data)"""
        self.set_mock_json(getmock, '{"responseCode":"1", "handle":"12.3456/sadlask"}')
        self.set_mock_json(putmock, '{"responseCode":"1", "handle":"31.74883/newpid"}')
        uuidmock.return_value = "newpid"
        self.testclass.simple_alias("12.3456/sadlask")
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/newpid",
            data='{"values": [{"index": 1, "type": "HS_ALIAS", '
            '"data": {"format": "string", "value": "12.3456/sadlask"}}, '
            '{"index": 100, "type": "HS_ADMIN", "data": {"format": "admin", '
            '"value": {"handle": "0.NA/31.74883", "index": 200, '
            '"permissions": "011111110011"}}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.uuid.uuid4")
    @patch("getepic.pid.pid.put")
    @patch("getepic.pid.pid.get")
    def test_simple_alias_fail_notexist(self, getmock, putmock, uuidmock):
        """Test that a simple alias-based handle request fails if aliased PID does not resolve"""
        self.set_mock_json(putmock, '{"responseCode":"1", "handle":"31.74883/newpid"}')
        uuidmock.return_value = "newpid"
        getmock().raise_for_status.side_effect = HTTPError("Error error error!")
        self.assertRaises(HTTPError, self.testclass.simple_alias, *("12.3456/sadlask",))
        putmock.assert_not_called()
        self.set_mock_json(getmock, '{"responseCode":"401"}')
        getmock.reset_mock()
        self.assertRaises(
            pid.HandleClientException,
            self.testclass.simple_alias,
            *("12.3456/sadlask",),
        )
        putmock.assert_not_called()

    @patch("getepic.pid.pid.uuid.uuid4")
    @patch("getepic.pid.pid.put")
    def test_simple_type_ok(self, putmock, uuidmock):
        """Test that the simple type request works as expected"""
        self.set_mock_json(putmock, '{"responseCode":"1", "handle":"31.74883/newpid"}')
        uuidmock.return_value = "newpid"
        self.testclass.simple_type("EMAIL", "bloep@blup.no")
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/newpid",
            data='{"values": [{"index": 1, "type": "EMAIL", '
            '"data": {"format": "string", "value": "bloep@blup.no"}}, '
            '{"index": 100, "type": "HS_ADMIN", "data": {"format": "admin", '
            '"value": {"handle": "0.NA/31.74883", "index": 200, '
            '"permissions": "011111110011"}}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.delete")
    def test_delete_handle_ok(self, delmock):
        """Test that deleting handle calls the right URL"""
        self.testclass.delete_handle(suffix="12345", agree=True)
        self.logger.warning.assert_called_once()
        delmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/12345",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )
        self.logger.reset_mock()
        self.testclass.delete_handle(suffix="12.3456/78520", agree=True)
        self.logger.warning.assert_called_once()

        delmock.assert_called_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/78520",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.delete")
    def test_delete_handle_fail(self, delmock):
        """Test that deleting handle fails if not explicitly allowed"""
        self.assertRaises(UserWarning, self.testclass.delete_handle, *("handle",))
        self.logger.error.assert_called_once()
        delmock.assert_not_called()

    @patch("getepic.pid.pid.put")
    def test_modify_handle_ok(self, putmock):
        """Test that modifying handle calls right URL and data"""
        self.testclass.add_hdl_entry("someentry")
        self.testclass.modify_handle("suffix")
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/suffix?index=various",
            data='{"values": [{"index": 1, "type": "URL", "data": '
            '{"format": "string", "value": "someentry"}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )
        putmock.reset_mock()
        self.testclass.modify_handle("12.345/suffix")
        putmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles/31.74883/suffix?index=various",
            data='{"values": [{"index": 1, "type": "URL", '
            '"data": {"format": "string", "value": "someentry"}}]}',
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.put")
    def test_modify_handle_fail_zeroids(self, putmock):
        """Test that modifying handle fails if no ids specified"""
        self.assertRaises(
            pid.HandleClientException, self.testclass.modify_handle, *("suffix",)
        )
        self.logger.error.assert_called_once()
        putmock.assert_not_called()

    @patch("getepic.pid.pid.delete")
    def test_delete_session_ok(self, delmock):
        """Test that session is correctly deleted"""
        self.testclass.session = "1232131"
        delmock().status_code = 204
        delmock.reset_mock()
        self.testclass.delete_session()
        delmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/sessions/this",
            data=None,
            cert=None,
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": 'Handle sessionId="1232131"',
            },
        )

    @patch("getepic.pid.pid.delete")
    def test_delete_sessionfail_nosess(self, deletemock):
        """Test that session deletion fails if no session is active"""
        self.assertRaises(pid.HandleClientException, self.testclass.delete_session)
        self.logger.error.assert_called_once()
        deletemock.assert_not_called()

    @patch("getepic.pid.pid.delete")
    def test_delete_sessionfail_noerrcode(self, delmock):
        """Test that session fails if http return code is not 204."""
        delmock().status_code = 205
        self.testclass.session = "1234"
        self.assertRaises(HandleClientException, self.testclass.delete_session)
        self.logger.error.assert_called_once()

    @patch("getepic.pid.pid.get")
    def test_list_my_handles(self, getmock):
        """Test listing of own handles calls correct URL"""
        self.testclass.list_my_handles()
        getmock.assert_called_once_with(
            "https://myhandle.provider.ext:1982/api/handles?prefix=31.74883",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    @patch("getepic.pid.pid.get")
    def test_find_handle(self, getmock):
        """Test handle query calls correct URL"""
        self.testclass.find_handle("12.345/6789")
        getmock.assert_called_once_with(
            "https://hdl.handle.net/api/handles/12.345/6789",
            data=None,
            cert=("/mycertifi.cate", "/myprivate.key"),
            headers={
                "Content-Type": "application/json",
                "Accept": "application/json",
                "Authorization": "Handle clientCert=true",
            },
        )

    def tearDown(self):
        self.testclass.session = None
        del self.testclass


if __name__ == "__main__":
    unittest.main()
