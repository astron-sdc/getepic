# SPDX-License-Identifier: Apache-2.0
"""Tests for the piddb module"""
import unittest
from unittest.mock import patch
from getepic import piddb

constrained_types = [
    "HS_ADMIN",
    "HS_VLIST",
    "HS_SECKEY",
    "HS_PUBKEY",
    "HS_SITE",
    "HS_SERV",
    "HS_ALIAS",
    "EMAIL",
    "URL",
    "URN",
    "INET_HOST",
    "10320/LO",
    "DESC"
]


class PidDbTest(unittest.TestCase):
    """Tests for the piddb module"""

    def setUp(self):
        self.database = piddb.PidDB("sqlite:///:memory:")
        self.logger = patch("getepic.piddb.piddb.logger").start()

    def test_datatypelist(self):
        """Test that the data type list covers all allowed types"""
        typelister = self.database.types.select().with_only_columns(
            [self.database.types.c.linktype]
        )
        with self.database.engine.connect() as conn:
            typelist = [val[0] for val in conn.execute(typelister).all()]
        self.assertListEqual(sorted(typelist), sorted(constrained_types))

    def test_insert_ok(self):
        """Test that an insert of valid data results in the right table entries"""
        vals_url = ("12.3456/mysuff", "URL", "fake://fakeurl.fake", "Mycollec")
        vals_alias = (
            "88.3330/otherstuff",
            "HS_ALIAS",
            "12.3456/mysuff",
            "Othercollec",
            "Description",
        )
        vallist = [vals_url + (None,), vals_alias]
        self.database.insert(*vals_url)
        self.database.insert(*vals_alias)
        column_list = [
            self.database.pids.c.epic_pid,
            self.database.types.c.linktype,
            self.database.pids.c.target,
            self.database.collections.c.name,
            self.database.pids.c.description,
        ]
        joined_table = self.database.pids.join(self.database.collections).join(
            self.database.types
        )
        select_stmt = joined_table.select().with_only_columns(column_list)
        with self.database.engine.connect() as conn:
            allpids = conn.execute(select_stmt).all()
        self.assertListEqual(vallist, allpids)

    def test_insert_fail(self):
        """Test that an error is raised if the valuetype is non-existent"""
        vals = ("12.3456/mysuff", "FAKE", "fake://fakeurl.fake", "Mycollec")
        self.assertRaises(ValueError, self.database.insert, *vals)
        select_stmt = self.database.pids.select()
        with self.database.engine.connect() as conn:
            self.assertFalse(len(conn.execute(select_stmt).all()))
        self.logger.error.assert_called_once()


if __name__ == "__main__":
    unittest.main()
