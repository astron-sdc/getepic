# SPDX-License-Identifier: Apache-2.0
"""Tests for the getepic script"""
from unittest import TestCase
from unittest.mock import MagicMock, patch, mock_open, call
from textwrap import dedent
from getepic import getepic


class GetEpicTest(TestCase):
    """Tests for the getepic script"""

    def setUp(self):
        sleepmocker = patch("getepic.getepic.sleep")
        sleepmocker.start()
        self.logpatcher = patch("getepic.getepic.logger", new_callable=MagicMock)
        self.logmocker = self.logpatcher.start()

    @staticmethod
    def dedent_strip(inpstring):
        """Dedent and strip a probably triple-quoted string. This is to
        make file mocks that are not indented and do not start or end
        with empty lines but still look nice in code.

        Parameters
        ----------
        inpstring : str
            the string to dedent and strip.

        Returns
        -------
        str
            The same string, but dedented and stripped.
        """
        return dedent(inpstring).strip()

    def test_arguments_errors(self):
        """Test that erronomous arguments raise errors."""
        configfile = self.dedent_strip(
            """
        {
         "database_address": "Mydatabaseaddress"
        }
        """
        )
        with patch("builtins.open", new_callable=mock_open, read_data=configfile):
            with self.assertRaises(SystemExit) as exc_raised:
                with patch("sys.stderr") as std_err:
                    getepic.parse_arguments(
                        [
                            "-l",
                            "FAKE",
                            "-c",
                            "MyDemoCollection",
                            "config.json",
                        ]
                    )
                    std_err.assert_called_once()
                self.assertEqual(exc_raised.exception.code, 2)
            with self.assertRaises(SystemExit) as exc_raised:
                with patch("sys.stderr") as std_err:
                    getepic.parse_arguments(
                        [
                            "-t",
                            "FAKE",
                            "-c",
                            "MyDemoCollection",
                            "config.json",
                        ]
                    )
                    std_err.assert_called_once()
                self.assertEqual(exc_raised.exception.code, 2)
            with self.assertRaises(SystemExit) as exc_raised:
                with patch("sys.stderr"):
                    getepic.parse_arguments(
                        [
                            "-a",
                            "FAKE",
                            "-c",
                            "MyDemoCollection",
                            "config.json",
                        ]
                    )
                    std_err.assert_called_once()
                self.assertEqual(exc_raised.exception.code, 2)
            with self.assertRaises(SystemExit) as exc_raised:
                with patch("sys.stderr") as std_err:
                    getepic.parse_arguments(
                        [
                            "-t",
                            "FAKE",
                            "-b",
                            "NiceBatchFile.csv",
                            "config.json",
                        ]
                    )
                    std_err.assert_called_once()
                self.assertEqual(exc_raised.exception.code, 2)

    @patch("getepic.getepic.logging")
    def test_set_debugging(self, logmock):
        """Test that debugging is enabled when -d flag is set"""
        configfile = self.dedent_strip(
            """
        {
         "database_address": "Mydatabaseaddress"
        }
        """
        )
        with patch("builtins.open", new_callable=mock_open, read_data=configfile):
            getepic.parse_arguments(
                [
                    "-t",
                    "fakething.dat",
                    "-l",
                    "FAKE",
                    "-c",
                    "TheCollection",
                    "config.json",
                ]
            )
            logmock.basicConfig.assert_called_with(level=logmock.INFO)
            getepic.parse_arguments(
                [
                    "-t",
                    "fakething.dat",
                    "-l",
                    "FAKE",
                    "-c",
                    "TheCollection",
                    "config.json",
                    "-d",
                ]
            )
            logmock.basicConfig.assert_called_with(level=logmock.DEBUG)

    def test_json_input_ok(self):
        """Test that a json file as batch input parses ok"""
        jsonfile = self.dedent_strip(
            """
    [
        {"type":"BLANK", "target":"fakeurl"},
        {"type":"OTHERTYPE", "target":"othertgt"},
        {"type":"THIRDTYPE", "target":"file3"}
    ]
    """
        )
        with patch("builtins.open", new_callable=mock_open, read_data=jsonfile):
            output_datalist = getepic.read_batch_file("somefile.json", None)
        output_checklist = [
            {"type": "BLANK", "target": "fakeurl", "description": None},
            {"type": "OTHERTYPE", "target": "othertgt", "description": None},
            {"type": "THIRDTYPE", "target": "file3", "description": None},
        ]
        self.assertListEqual(output_datalist, output_checklist)

    def test_json_input_wrongnames_fail(self):
        """Test that an exception is raised if the json fields are not correctly named"""
        jsonfile = self.dedent_strip(
            """
    [
        {"wrong":"BLANK", "nope":"fakeurl"},
        {"other":"OTHERTYPE", "yeah":"othertgt"},
        {"none":"THIRDTYPE", "hi":"file3"}
    ]
    """
        )
        with patch("builtins.open", new_callable=mock_open, read_data=jsonfile):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.json", None)
            )
        self.logmocker.error.assert_called_once()

    def test_csv_input_ok(self):
        """Test that CSV input gets parsed correctly"""
        csv_1_head = self.dedent_strip(
            """
    target
    fake://fakeurl.nono
    fake://otherfake.indeed
    """
        )
        csv_1_nohead = self.dedent_strip(
            """
    fake://fakeurl.nono
    fake://otherfake.indeed
    """
        )
        csv_2_head = self.dedent_strip(
            """
    type, target, description
    FAKE, fake://fakeurl.nono, some stuff
    ALSO, fake://otherfake.indeed, other stuff
    """
        )
        csv_2_head_swp = self.dedent_strip(
            """
    target, type, description
    fake://fakeurl.nono, FAKE, some stuff
    fake://otherfake.indeed, ALSO, other stuff
    """
        )
        csv_multi_only_type = self.dedent_strip(
            """
    target, somefield, someotherfield
    fake://fakeurl.nono, FAKE, some stuff
    fake://otherfake.indeed, ALSO, other stuff
    """
        )
        output_checklist_multi = [
            {
                "type": "FAKE",
                "target": "fake://fakeurl.nono",
                "description": "some stuff",
            },
            {
                "type": "ALSO",
                "target": "fake://otherfake.indeed",
                "description": "other stuff",
            },
        ]
        output_checklist_single = [
            {"type": "FAKE", "target": "fake://fakeurl.nono", "description": None},
            {"type": "FAKE", "target": "fake://otherfake.indeed", "description": None},
        ]
        output_checklist_mixed = [
            {
                "target": "fake://fakeurl.nono",
                "somefield": "FAKE",
                "someotherfield": "some stuff",
                "type": "FAKE",
                "description": None,
            },
            {
                "target": "fake://otherfake.indeed",
                "somefield": "ALSO",
                "someotherfield": "other stuff",
                "type": "FAKE",
                "description": None,
            },
        ]

        with patch("builtins.open", new_callable=mock_open, read_data=csv_1_head):
            output_datalist = getepic.read_batch_file("somefile.csv", "FAKE")
        self.assertListEqual(output_datalist, output_checklist_single)
        self.logmocker.reset_mock()
        with patch("builtins.open", new_callable=mock_open, read_data=csv_1_nohead):
            output_datalist = getepic.read_batch_file("somefile.csv", "FAKE")
        self.assertListEqual(output_datalist, output_checklist_single)
        self.logmocker.warning.assert_called_once()
        self.logmocker.warning.reset_mock()

        with patch("builtins.open", new_callable=mock_open, read_data=csv_2_head):
            output_datalist = getepic.read_batch_file("somefile.csv", None)
        self.assertListEqual(output_datalist, output_checklist_multi)

        with patch("builtins.open", new_callable=mock_open, read_data=csv_2_head_swp):
            output_datalist = getepic.read_batch_file("somefile.csv", None)
        self.assertListEqual(output_datalist, output_checklist_multi)

        with patch(
            "builtins.open", new_callable=mock_open, read_data=csv_multi_only_type
        ):
            output_datalist = getepic.read_batch_file("somefile.csv", "FAKE")
        self.assertListEqual(output_datalist, output_checklist_mixed)
        self.logmocker.warning.assert_called_once()

    def test_csv_input_wrongnames_fail(self):
        """Test that a mulicol csv file without header names fails"""

        csv_2_noheaders = self.dedent_strip(
            """
    FAKE, fake://fakeurl.nono
    ALSO, fake://otherfake.indeed
    """
        )

        with patch("builtins.open", new_callable=mock_open, read_data=csv_2_noheaders):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.csv", None)
            )
        self.logmocker.error.assert_called_once()

    def test_csv_wrongcolumns_fail(self):
        """Test that CSV file without target column fails"""
        csv_3_head = self.dedent_strip(
            """
    hello, goodbye, extra
    fake://fakeurl.nono, URL, fail
    fake://otherfake.indeed, URL, failtoo
    """
        )
        with patch("builtins.open", new_callable=mock_open, read_data=csv_3_head):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.csv", None)
            )
        self.logmocker.error.assert_called_once()

    def test_batch_with_types_and_typeflag_fail(self):
        """Test parsing fails when types in batch file and on command line"""
        jsonfile = self.dedent_strip(
            """
    [
        {"type":"OTHERTYPE", "target":"othertgt"},
        {"type":"THIRDTYPE", "target":"file3"}
    ]
    """
        )
        csv_1_head = self.dedent_strip(
            """
    target
    fake://fakeurl.nono
    fake://otherfake.indeed
    """
        )
        csv_1_nohead = self.dedent_strip(
            """
    fake://fakeurl.nono
    fake://otherfake.indeed
    """
        )
        csv_2_head = self.dedent_strip(
            """
    type, target
    FAKE, fake://fakeurl.nono
    ALSO, fake://otherfake.indeed
    """
        )
        with patch("builtins.open", new_callable=mock_open, read_data=jsonfile):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.csv", "FAKE")
            )
        self.logmocker.error.assert_called_once()
        self.logmocker.reset_mock()
        with patch("builtins.open", new_callable=mock_open, read_data=csv_1_head):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.csv", None)
            )
        self.logmocker.error.assert_called_once()
        self.logmocker.reset_mock()
        with patch("builtins.open", new_callable=mock_open, read_data=csv_1_nohead):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.csv", None)
            )
        self.logmocker.warning.assert_called_once()
        self.logmocker.error.assert_called_once()
        self.logmocker.reset_mock()

        with patch("builtins.open", new_callable=mock_open, read_data=csv_2_head):
            self.assertRaises(
                ValueError, getepic.read_batch_file, *("somefile.csv", "FAKE")
            )
        self.logmocker.error.assert_called_once()

    @patch("getepic.getepic.pid")
    @patch("getepic.getepic.piddb")
    def test_single_request_ok(self, mockpiddb, mockpid):
        """Test that a request calls the right functions for all types"""
        configfile = self.dedent_strip(
            """
    {
     "database_address": "Mydatabaseaddress"
    }
    """
        )
        mockpid.HandleClient().simple_type.return_value = "12.345/AAA"
        with patch("builtins.open", new_callable=mock_open, read_data=configfile):
            args = getepic.parse_arguments(
                [
                    "-l",
                    "FAKE",
                    "-t",
                    "fake://fakeurl.nono",
                    "-a",
                    "DESCRIPTION",
                    "-c",
                    "MyDemoCollection",
                    "config.json",
                ]
            )
        getepic.request_and_register_pid(args)
        mockpid.assert_has_calls(
            [
                call.HandleClient("config.json"),
                call.HandleClient().start_session(),
                call.HandleClient().simple_type("FAKE", "fake://fakeurl.nono"),
                call.HandleClient().delete_session(),
            ]
        )
        mockpiddb.assert_has_calls(
            [
                call.PidDB("Mydatabaseaddress"),
                call.PidDB().insert(
                    "12.345/AAA",
                    "FAKE",
                    "fake://fakeurl.nono",
                    "MyDemoCollection",
                    "DESCRIPTION",
                ),
            ]
        )
        mockpid.reset_mock()
        mockpiddb.reset_mock()
        with patch("builtins.open", new_callable=mock_open, read_data=configfile):
            args_nodesc = getepic.parse_arguments(
                [
                    "-l",
                    "FAKE",
                    "-t",
                    "fake://fakeurl.nono",
                    "-c",
                    "MyDemoCollection",
                    "config.json",
                ]
            )
            getepic.request_and_register_pid(args_nodesc)

        mockpid.assert_has_calls(
            [
                call.HandleClient("config.json"),
                call.HandleClient().start_session(),
                call.HandleClient().simple_type("FAKE", "fake://fakeurl.nono"),
                call.HandleClient().delete_session(),
            ]
        )
        mockpiddb.assert_has_calls(
            [
                call.PidDB("Mydatabaseaddress"),
                call.PidDB().insert(
                    "12.345/AAA",
                    "FAKE",
                    "fake://fakeurl.nono",
                    "MyDemoCollection",
                    None,
                ),
            ]
        )

    @patch("getepic.getepic.pid")
    @patch("getepic.getepic.piddb")
    def test_batch_request_ok(self, piddbmock, pidmock):
        """Test that a batch request calls the right functions for each option"""
        jsonfile = self.dedent_strip(
            """
    [
        {"type":"URL", "target":"fakeurl"},
        {"type":"HS_ALIAS", "target":"othertgt"},
        {"type":"THIRDTYPE", "target":"file3"}
    ]
    """
        )
        configfile = self.dedent_strip(
            """
    {
     "database_address": "Mydatabaseaddress"
    }
    """
        )
        pidmock.HandleClient().simple_type.return_value = "12.345/AAA"
        pidmock.HandleClient().simple_url.return_value = "45.678/BBB"
        pidmock.HandleClient().simple_alias.return_value = "78.900/CCC"
        with patch("builtins.open", new_callable=mock_open, read_data=configfile):
            args = getepic.parse_arguments(
                ["-b", "batchfile.json", "config.json", "-c", "MyDemoCollection"]
            )
        with patch("builtins.open", new_callable=mock_open, read_data=jsonfile):
            getepic.request_and_register_pid(args)
        pidmock.assert_has_calls(
            [
                call.HandleClient("config.json"),
                call.HandleClient().start_session(),
                call.HandleClient().simple_url("fakeurl"),
                call.HandleClient().simple_alias("othertgt"),
                call.HandleClient().simple_type("THIRDTYPE", "file3"),
                call.HandleClient().delete_session(),
            ]
        )
        piddbmock.assert_has_calls(
            [
                call.PidDB("Mydatabaseaddress"),
                call.PidDB().insert(
                    "45.678/BBB", "URL", "fakeurl", "MyDemoCollection", None
                ),
                call.PidDB().insert(
                    "78.900/CCC", "HS_ALIAS", "othertgt", "MyDemoCollection", None
                ),
                call.PidDB().insert(
                    "12.345/AAA", "THIRDTYPE", "file3", "MyDemoCollection", None
                ),
            ]
        )
