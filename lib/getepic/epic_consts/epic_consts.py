# SPDX-License-Identifier: Apache-2.0
"""Module that holds some constants for EPIC PID generations.
constrained_types: List of the link types supported by the EPIC PID service.
hdl_response_values: Dict to map the response code from the service to
actually human-readable messages. 
constrained_formats: List of formats for links.
For more details, see the documentation: https://www.handle.net/tech_manual/HN_Tech_Manual_9.pdf"""
constrained_types = [
    "HS_ADMIN",
    "HS_VLIST",
    "HS_SECKEY",
    "HS_PUBKEY",
    "HS_SITE",
    "HS_SERV",
    "HS_ALIAS",
    "EMAIL",
    "URL",
    "URN",
    "INET_HOST",
    "10320/LO",
    "DESC"
]

hdl_response_values = {
    1: "Success",
    2: "Error",
    3: "Server Too Busy",
    4: "Protocol Error",
    5: "Operation Not Supported",
    6: "Recursion Count Too High",
    7: "Server Read-only",
    100: "Handle Not Found",
    101: "Handle Already Exists",
    102: "Invalid Handle",
    200: "Values Not Found",
    201: "Value Already Exists",
    202: "Invalid Value",
    300: "Out of Date Site Info",
    301: "Server Not Responsible",
    302: "Service Referral",
    303: "Prefix Referral",
    400: "Invalid Admin",
    401: "Insufficient Permissions",
    402: "Authentication Needed",
    403: "Authentication Failed",
    404: "Invalid Credential",
    405: "Authentication Timed Out",
    406: "Authentication Error",
    500: "Session Timeout",
    501: "Session Failed",
    502: "Invalid Session Key",
    504: "Invalid Session Setup Request",
    505: "Session Duplicate Msg Rejected",
}

constrained_formats = ["string", "base64", "hex", "admin", "vlist", "site", "key"]
