#!/usr/bin/env python3
# SPDX-License-Identifier: Apache-2.0
"""Module for requesting EPIC PIDs and storing them in a database."""
import argparse
import csv
import json
import sys
from getepic import piddb
from getepic import pid
import logging
from time import sleep

logger = logging.getLogger(__name__)


def parse_arguments(argstr=None):
    """Parse the arguments provided to the command using an argparser.

    Parameters
    ----------
    argstr : list, optional
        The list of arguments, by default None

    Returns
    -------
    argparse.Namespace
        The output of argparse.parse_args for the input, where we also add the
        database details from the configuration file.
    """
    parser = argparse.ArgumentParser(
        usage="Generate EPIC PIDs and put them in a database. "
        "The configuration can be put in a json file. "
        "You can either create a single link using the -l "
        "and -t flags, or generate a list. The list can be "
        "either a csv file or a json file. The names of the columns "
        "to be parsed are 'type' and 'target'. If all links have "
        "the same linktype, you can also provide a list of targets "
        "and configure the type it with the -l flag.",
    )
    linkdefs = parser.add_mutually_exclusive_group()
    parser.add_argument("configfile", help="Configuration file (json).")
    linkdefs.add_argument("-t", "--target", help="Target of the PID to generate")
    linkdefs.add_argument(
        "-b",
        "--batchfile",
        help="Batch definition file. This file can be either json, "
        "csv or a single list of targets. If json or csv, the "
        "header keys should be called 'target' (req.), 'type' and 'description'."
        "Description is fully optional. If 'type' is not a column, it should be set "
        "using the -l argument.",
    )
    parser.add_argument("-l", "--linktype", help="Linktype of the PID(s) to generate.")
    parser.add_argument(
        "-c",
        "--collection",
        help="Collection the links created during the run belong to.",
        required=True,
    )
    parser.add_argument(
        "-d",
        "--debug",
        help="Activate debug logging.",
        action="store_true",
    )
    parser.add_argument("-a", "--description", help="Description of the handle.")
    args = parser.parse_args(argstr)
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    logger.debug("Arguments parsed. Checking for issues...")
    if not (args.batchfile or args.target):
        parser.error("Need at least one of -t/--target or -b/--batchfile")
    if args.target and not args.linktype:
        parser.error(
            "When manually specifying a target, you also need "
            "to specify the link type with -l/--linktype"
        )
    with open(args.configfile, encoding="UTF-8") as cfh:
        config_data = json.load(cfh)
    args.db_address = config_data["database_address"]
    logger.debug(f"Arguments parsed. Argument list: {args}")
    return args


def check_for_header(firstline):
    """Check if the first line of the csv is a header, and whether the header contains
       at least a 'target' and potentially a 'type' and ;description.

    Parameters
    ----------
    firstline : list
        First line read by csv.reader.

    Returns
    -------
    bool
        If True, the headers are ok.
        If False, there was only one column, without header (assume this is a list of targets).

    Raises
    ------
    ValueError
        If there is no 'target' among the header fields.
    """
    logger.debug("Checking the headers.")
    if len(firstline) == 1:
        if firstline[0] != "target":
            logger.warning(
                "This looks like a list of items without header. "
                "Will act accordingly. Consider adding the 'target' "
                "header to suppress this warning."
            )
            return False
        else:
            logger.debug("Single-line CSV with correct header field detected.")
            return True

    if "target" not in firstline:
        logger.error(
            "The CSV file does not have a 'target' header " "field. This is required!"
        )
        raise ValueError(
            "Multicolumn CSV without the 'target' header " "is not accepted."
        )
    if ("type" not in firstline) and ("description" not in firstline):
        logger.warning(
            "The CSV has more than one column, but none "
            "of the other columns is called 'type' or 'description' "
            "only the 'target' column will be used. If this is not your intention "
            "please break and restart (will sleep for 5 sec now)."
        )
        sleep(5)
    logger.debug("Multicolumns CSV found with at least the required header field.")
    return True


def validate_read_csv(fhand):
    """Perform some checks on a csv file and read it:
        1. Run check_for_header on the first line
        2a. If this is not a list of files: read it with a DictReader
        2b. Else turn it in a list of dicts with key 'target'
        ...

     Parameters
     ----------
     fhand : file handle
         The file handle of the csv file

     Returns
     -------
    list (of dicts)
        A list of dicts of the csv data.

    """
    logger.debug("Validating the CSV file provided...")
    csv_rawdat = fhand.readlines()
    firstline = next(csv.reader(csv_rawdat, delimiter=",", skipinitialspace=True))
    if check_for_header(firstline):
        logger.debug("Parsing CSV data using a dictreader...")
        csv_data = [
            dat
            for dat in csv.DictReader(csv_rawdat, delimiter=",", skipinitialspace=True)
        ]
    else:
        logger.debug(
            "Since this is a single line: parsing CSV data without a dictreader..."
        )
        csv_data = [
            {"target": tgt[0]}
            for tgt in csv.reader(csv_rawdat, delimiter=",", skipinitialspace=True)
        ]
    return csv_data


def check_apply_linktype(input_data, linktype):
    """Check if the linktype is either set in the input dict, or as a command line parameter.
    If the latter is the case, add linktypes to the dicts in the list.

    Parameters
    ----------
    input_data : list (of dicts)
        The dictionary with data to validate.
    linktype : str or None
        linktype that is set as a command line argument.

    Raises
    ------
    ValueError
        If the type is neither set in the dict, nor on the command line.
        If the type is set both in the dict, as in the command line.
    """
    logger.debug("Applying link type to input data...")
    try:
        input_data[0]["type"]
    except (TypeError, KeyError) as kterr:
        if not linktype:
            logger.error(
                "Please use the -l/--linktype flag if "
                "there are no linktypes in the batch file."
            )
            raise ValueError(
                "Please specify a link type if the batch file only contains target data."
            ) from kterr
        for dent in input_data:
            logger.debug(f"Adding linktype {linktype} to all entries in the list.")
            dent["type"] = linktype
        return
    if linktype:
        logger.error(
            "Link type is set both in the dict, as on the command line. "
            "Please pick one."
        )
        raise ValueError(
            "Link type specified twice. TThis should be specified only once."
        )


def add_descriptions(input_data):
    """Add a 'description' key to each dict in the input data that does not
       have a description. the value of this entry is None.

    Parameters
    ----------
    input_data : list (of dicts)
        List of items to which to append the value.
    """
    logger.debug("Adding descriptions to entries that have none...")
    for datline in input_data:
        try:
            datline["description"]
        except KeyError:
            datline["description"] = None


def read_batch_file(fname, linktype):
    """Try to read and validate the input file as json.
    If that fails, try to read and validate it as csv.

    Parameters
    ----------
    fhand : file handle
        The file handle of the json file.
    linktype : str
        The link type command line setting. (for validation)

    Returns
    -------
    list
        list of dicts with the request data.
    """
    logger.info(f"Reading batch file {fname}.")
    logger.debug(f"linktype {linktype} provided")
    try:
        with open(fname, encoding="UTF-8") as bfh:
            batch_data = json.load(bfh)
    except json.decoder.JSONDecodeError:
        with open(fname, encoding="UTF-8") as bfh:
            batch_data = validate_read_csv(bfh)
        logger.info("Parsing input file as CSV/filelist.")
    else:
        logger.info("Parsing input file as JSON.")
    check_apply_linktype(batch_data, linktype)
    add_descriptions(batch_data)
    logger.debug("Preprocessing of batch file data done.")
    return batch_data


def request_and_register_pid(args):
    """Take input from either the command line or a file, request pid
    and register in the database.

    Parameters
    ----------
    args : argparse.Namespace
        The command line options as provided by the parse_arguments function.
    """
    logger.info("Starting PID request(s).")
    if args.batchfile:
        rq_data = read_batch_file(args.batchfile, args.linktype)
    else:
        if args.description:
            descstr = f" and description: {args.description}"
        else:
            descstr = ""
        logger.info(
            f"Requesting single PID for object {args.target},  linktype {args.linktype}"
            + descstr
        )
        rq_data = [
            {
                "type": args.linktype,
                "target": args.target,
                "description": args.description,
            }
        ]

    logger.debug("Starting HandleClient in session mode...")
    handle_client = pid.HandleClient(args.configfile)
    handle_client.start_session()
    logger.debug("Starting PidDB...")
    pid_db = piddb.PidDB(args.db_address)
    total_items = len(rq_data)
    if total_items <= 50:
        progress = False
    else:
        progress = True
    if args.batchfile:
        if not progress:
            logger.info("Less than 50 items requested. Will not give progress updates.")
            progress = False
        else:
            logger.info(
                "More than 50 items requested. Will give progress updates "
                "for every 50 items completed."
            )

            pct_done = 0
            pct_of_50 = (50.0 / total_items) * 100
    for data_idx, data_object in enumerate(rq_data):
        if data_object["type"] == "URL":
            epic_pid = handle_client.simple_url(data_object["target"])
        elif data_object["type"] == "HS_ALIAS":
            epic_pid = handle_client.simple_alias(data_object["target"])
        else:
            epic_pid = handle_client.simple_type(
                data_object["type"], data_object["target"]
            )
        pid_db.insert(
            epic_pid,
            data_object["type"],
            data_object["target"],
            args.collection,
            data_object["description"],
        )
        if (data_idx + 1) % 50 == 0 and progress:
            pct_done += pct_of_50
            logger.info(f"{data_idx + 1} done ({pct_done:.2f}%).")
    logger.info("All done!")
    logger.debug("Deleting session...")
    handle_client.delete_session()


def main():
    """Main function. Parses arguments via the parse_arguments function and then
    requests and registers the PIDs based on this data.
    """
    args = parse_arguments()
    logger.info("Initiating request(s)...")
    request_and_register_pid(args)


if __name__ == "__main__":
    sys.exit("please execute the bin version of getepic")
