# SPDX-License-Identifier: Apache-2.0
"""Module for the management of EPIC PID handles.
The module variables are house keeping values.
The main object is the HandleClient, which can be used
to request and edit EPIC PID handles.
"""
import uuid
import json
import logging
from urllib.parse import urljoin
from requests import post, get, put, delete
from requests.exceptions import HTTPError
from getepic.epic_consts import (
    hdl_response_values,
    constrained_types,
    constrained_formats,
)

logger = logging.getLogger(__name__)


class HandleClientException(Exception):
    """Exceptions from the Handle client."""


class HandleClient:
    """Client to request EPIC PID handles using the REST API (Only
    tested against the surfsara API). For more details on the
    API see: https://www.handle.net/tech_manual/HN_Tech_Manual_9.pdf
    The SURF documentation on EPIC handles can be found here:
    https://userinfo.surfsara.nl/systems/epic-pid

    In the documentation we will use the terms prefix and suffix
    as defined here. A handle consists of a prefix
    (looking something like 21.T12995 for the test prefix)
    and a suffix (in locally unique string, often a UUID).
    The full handle looks like prefix/suffix (e.g. 21.T12995/my_suffix_uuid would
    be a valid PID).

    Raises
    ------
    HandleClientException
        Exceptions raised when the request or session are invalid.
    """

    @staticmethod
    def _parse_reqres(response):
        """Parse requests response for http code and json-wrapped responseCode.
        Raise exception when either the http return code is not ok (as defined
        by the raise_for_status() fuction) or the json status is not 1.

        Parameters
        ----------
        response : requests.models.Response
            Response from a requests object.

        Raises
        ------
        HandleClientException
            When the code in the json response of the server is not 1,
            this is raised as an exception. Note that this precedes any
            http status code based exceptions.
        requests.exceptions.HTTPError:
            When the http connection returns a not-ok error code,
            this exception is raised (as per the raise_for_status() function)
        """
        logger.debug("Parsing requests response...")
        try:
            rsp_json = response.json()
            rcode = int(rsp_json["responseCode"])
        except json.JSONDecodeError:
            logger.debug(
                "Response could not be parsed as json. Will rely on http return code."
            )
            rcode = 1
        except KeyError:
            logger.debug(
                "JSON response did not contain a responseCode. Will rely on http return code."
            )
            rcode = 1
        if rcode != 1:
            logger.error(
                f"The response from a call to the API returned error {hdl_response_values[rcode]}"
            )
            raise HandleClientException(
                f"Response error: {hdl_response_values[rcode]}."
                f"Full response: {response.text}"
            )
        response.raise_for_status()
        return response

    @staticmethod
    def _suffix_fixup(suffix):
        """Simple function to automatically fix when someone
        provided a full PID in stead of only the suffix. For
        instance 21.13345/uufhsajks-asdjn will be parsed to uufhsajks-asdjn.

        Parameters
        ----------
        suffix : str
            The suffix value to check and potentially fix.

        Returns
        -------
        str
            The actual suffix of the PID.
        """
        logger.debug("Check if we received a suffis, or a postfix/suffix string...")
        if suffix.split("/")[-1] != suffix:
            logger.debug(f"String {suffix} looks like a postfix/suffix. Fixing...")
            suffix = suffix.split("/")[-1]
        logger.debug(f"Will return the suffix {suffix}.")
        return suffix

    def _request(self, method, urlpath, data=None, sessauth=None, abs=False):
        """Wrapper around the requests to be executed for the handler.
        This will detect if a session is set, and authenticate with either certificates
        (from the configuration) or the session id.
        Post is a special case because it is only used for obtaining the session id, and does
        not need to provide any credentials or data for that.

        Parameters
        ----------
        method : function
            The requests function (post, get, delete or put) to execute
        urlpath : str
            URL to provide as input to hte request
        data : str (or None), optional
            json string representation of the data to provide to the method, by default None
        sessauth : str (or None), optional
            if this is a session authentication request, add this session id to the header for
            authentication

        Returns
        -------
        requests.models.Response
            The response of the request.
        """
        if not abs:
            url = urljoin(self.handle_server_root, urlpath)
        else:
            url = urlpath
        logger.debug(f"Will send request to {url}...")
        if method is post:
            logger.debug("Request type is POST.")
            response = post(url)
            response.raise_for_status()
            logger.debug("Call returned ok. Returning respons.")
            return response
        logger.debug("Setting default headers...")
        headers = {"Content-Type": "application/json", "Accept": "application/json"}
        if self.session:
            logger.debug("Running in a session, setting session headers too...")
            headers["Authorization"] = f'Handle sessionId="{self.session}"'
            cert = None
        else:
            logger.debug(
                "Not running in a session, setting certificate auth headers too..."
            )
            headers["Authorization"] = "Handle clientCert=true"
            cert = self.certpack
        if sessauth is not None:
            logger.debug(
                "Call for session authorisation. Adding session ID to headers."
            )
            headers["Authorization"] += f', Handle sessionId="{sessauth}"'
        logger.debug(
            f"All set, now sending {method} request to {url} "
            f"with data {data}, certificate {cert} and headers {headers}..."
        )
        response = method(url, data=data, cert=cert, headers=headers)
        self._parse_reqres(response)
        logger.debug("Call returned ok. Returning respons.")
        return response

    def __init__(self, config):
        """Setup the class based on a json configuration file.

        Parameters
        ----------
        config : str
         path to a json file with the configuration parameters. The required parameters are:
         "handle_server_url": URL to the handle server
         "handle_server_port" : port to connect to on the handle server
         "prefix": prefix to manage using this client
         "private_key": path to the private key for authentication
         "certificate_only": path to the certificate for authentication
         An example file (settings_example.json) is included in the repository.
        """
        with open(config, encoding="utf-8") as jcf:
            configdict = json.load(jcf)
        logger.debug(f"Configuration keys read in: {configdict}.")
        handle_server_url = configdict["handle_server_url"]
        handle_server_port = configdict["handle_server_port"]
        self.handle_server_root = handle_server_url + ":" + handle_server_port
        self.prefix = configdict["prefix"]
        private_key = configdict["private_key"]
        certificate_only = configdict["certificate_only"]
        self.certpack = (certificate_only, private_key)
        self.indices = None
        self.handledata = None
        self.session = None
        self.writable = None
        self.reset_handledata()

    def reset_handledata(self):
        """Reset the handle data (i.e. the indices of the PID).
        This function can either be executed after a successful
        """
        logger.debug("Resetting handle data...")
        self.indices = []
        self.handledata = {"values": self.indices}
        self.writable = False

    def start_session(self):
        """Start a session if none is already present, and authenticate to it."""
        logger.debug("Starting session...")
        if self.session:
            logger.info("Session already active. Not starting a new one.")
            return
        # First create a session
        logger.debug("Requesting session ID...")
        session_resp = self._request(post, "/api/sessions")
        session_data = session_resp.json()
        session_id = session_data["sessionId"]

        # Second authenticate session
        logger.debug(f"Session id {session_id} obtained. Authenticating...")
        try:
            authresp = self._request(put, "/api/sessions/this", sessauth=session_id)
        except HandleClientException:
            self.session = None
            raise
        except HTTPError:
            self.session = None
            raise
        logger.debug("Session authentication returned...")
        logger.debug(f"Authentication response is: {authresp.text}")
        try:
            autstat = authresp.json()["authenticated"]
        except KeyError as kerr:
            logger.error("No authentication information in response.")
            raise HandleClientException("Authentication failed.") from kerr
        if autstat != True:
            logger.error("Authentication information in response is not True.")
            raise HandleClientException("Authentication failed.")
        self.session = session_id
        logger.debug("Session successfully authenticated.")

    def request_handle(self, suffix=None):
        """Request an EPIC PID using the data in the object.
        This can only happen when at least one entry with index 1 is present (by specification).
        If write is successful, handle data is reset (see reset_handledata).
        The common practice to randomise the suffix of the handle by
        using a UUID is used in this function.

        Parameters
        ----------
        suffix : str or None
            The suffix to request. 
            If None (which is strongly adviced) a UUID will be generated for it, 
            by default None

        Returns
        -------
        str
            The handle, as returned by the handle service (which should match the requested handle).

        Raises
        ------
        HandleClientException
            If the handle to be requested does not have an entry
            with index 1, a HandleClientException will be raised.
        """
        logger.debug(
            f"Requesting handle for current object with data {self.handledata}"
        )
        if not self.writable:
            raise HandleClientException(
                "Handle is not writable, probably because no entry has ID 1."
            )
        if not suffix:
            logger.debug("So suffix specified, so generating a suffix.")
            suffix = str(uuid.uuid4())
        logger.debug(f"Using suffix {suffix} for handle")
        self.add_hdl_entry(
            {
                "handle": f"0.NA/{self.prefix}",
                "index": 200,
                "permissions": "011111110011",
            },
            index=100,
            valtype="HS_ADMIN",
            dataformat="admin",
        )
        logger.debug(f"Admin entry added for prefix {self.prefix}.")
        handlejson = json.dumps(self.handledata)
        logger.debug(f"Requesting handle for data {handlejson}...")
        req_resp = self._request(
            put, f"/api/handles/{self.prefix}/{suffix}", data=handlejson
        )
        handle = req_resp.json()["handle"]
        logger.debug(f"Request successful ({handle}). Resetting handle data.")
        self.reset_handledata()
        return handle

    def add_hdl_entry(
        self, datavalue, index=1, valtype="URL", dataformat="string", overwrite=False
    ):
        """Add entry to the handle to be requested. If not explicitly specified
        this function will not overwrite existing indices.

        Parameters
        ----------
        datavalue : str
            Value of the field. How this looks fully depends on the value type.
        index : int, optional
            The index of the entry to add. All handles should at least have
            an entry with ID 1 (and one with ID 100 which is autogenerated by this
            module at write time). By default 1
        valtype : str, optional
            Value type for the entry, by default "URL".
            See documentation and the constrained_types class variable for allowed types.
        dataformat : str, optional
            Data format. One of string, base64, hex, admin, vlist,
            site or key. By default "string"
        overwrite : bool, optional
            If true, existing entries will be overwritten (and a warning raised).
            If False, an exception will be raised. By default False

        Raises
        ------
        HandleClientException
            If the valtype of dataformat is not in the list
            of allowed values, this exception will be raised.
            If the index already exists and overwrite is false,
            this exception will also be raised.
        """
        # pylint: disable=too-many-arguments
        # This function has more than pylint likes, but this is conscious
        logger.debug(
            "Adding data to handle request with data "
            f"{datavalue}, index {index}, valtype {valtype}, "
            f"dataformat {dataformat} and overwrite {overwrite}."
        )
        if valtype not in constrained_types:
            raise HandleClientException("Value type {valtype} is not allowed.")
        if dataformat not in constrained_formats:
            raise HandleClientException("Value type {dataformat} is not allowed.")
        for listidx, idx in enumerate(self.indices):
            logger.debug("Checking if index does not already exist.")
            if idx["index"] == index:
                if overwrite:
                    logger.warning(
                        f"Index {idx} already exists in handle. Will overwrite"
                    )
                    logger.debug("replacing index {listidx} that contains data {idx}.")
                    self.indices.pop(listidx)
                else:
                    raise HandleClientException(
                        f"Index {idx['index']} already exists in handle. This "
                        "may be caused by a failing write in the past, or you "
                        "actually intended this. Use overwrite flag if you want "
                        "to overwrite, or use the reset_handledata function in "
                        "this module to start all clean!"
                    )
        logger.debug("Appending indices to the list...")
        self.indices.append(
            {
                "index": index,
                "type": valtype,
                "data": {"format": dataformat, "value": datavalue},
            }
        )
        if index == 1:
            logger.debug("Index was 1, so making the handle writable.")
            self.writable = True

    def simple_url(self, url):
        """Simple function to create a PID referring to a URL.
        Note that not validation is performed as to whether the URL actually exists!

        Parameters
        ----------
        url : str
            The full URL of the object to request a PID for.

        Returns
        -------
        str
            The handle, as returned by the handle service (which should match the requested handle).
        """
        logger.debug(f"Requesting PID for URL {url}")
        self.add_hdl_entry(url)
        return self.request_handle()

    def simple_alias(self, alias):
        """Simple function to create a PID acting as an alias for another PID.
        Note that not validation is performed as to whether the PID actually exists!

        Parameters
        ----------
        alias : str
            The PID to which this will be an Alias.

        Returns
        -------
        str
            The handle, as returned by the handle service (which should match the requested handle).
        """
        logger.debug(f"Requesting PID alias for PID {alias}")
        self.find_handle(alias)  # fails if it does not exist
        self.add_hdl_entry(alias, valtype="HS_ALIAS")
        return self.request_handle()

    def simple_type(self, ltype, target):
        """Create a simple PID link for any type, without specific settings.

        Parameters
        ----------
        ltype : str
            Link type. See documentation and the constrained_types class variable for allowed types.

        target : target
            Target of the link. How this looks fully depends on the value type.

        Returns
        -------
        [type]
            [description]
        """
        logger.debug(f"Requesting PID of type {ltype} for object {target}")
        self.add_hdl_entry(target, valtype=ltype)
        return self.request_handle()

    def delete_handle(self, suffix, agree=False):
        """Delete a handle from your prefix. Please note that since the P in PID means
        "persistent" it his extremely bad behaviour to actually delete handles. This
        function is deactivated by default to make sure you only use it if you know what
        you are doing!

        Parameters
        ----------
        suffix : str
            The suffix of the handle that you want to delete.
        agree : bool, optional
            If True you know what you are doing and the code will execute.
            If False a UserWarning will be raised. By default False
            In either case, a warning will be printed.

        Raises
        ------
        UserWarning
            If you do not explicitly agree, this function will bail out with a UserWarning.
        """
        logger.debug(f"Deletion of {suffix} requested. Agree flag is {agree}")
        if not agree:
            logger.error("Handle deletion requested without explicit approval.")
            raise UserWarning(
                "Deleting handles is generally not a good idea. "
                "Please invoke with agree=True"
            )
        logger.warning(
            "Deleting handles is generally not a good idea. "
            "Please proceed with care!",
        )
        suffix = self._suffix_fixup(suffix)
        self._request(delete, f"api/handles/{self.prefix}/{suffix}")

    def modify_handle(self, suffix):
        """Apply modifications an existing handle

        Parameters
        ----------
        suffix : str
            The suffix of the handle to be modified
            (within the scope of the prefix managed by the client)

        Raises
        ------
        HandleClientException
            This exception will be raised if no data
            is present in the object when requesting a change.
        """
        logger.debug(f"Request to apply modifications to handle with suffix {suffix}")
        suffix = self._suffix_fixup(suffix)
        if len(self.indices) == 0:
            logger.error("Request to apply changes without changes being present.")
            raise HandleClientException(
                "You have to add indices before modifying a handle!"
            )
        handlejson = json.dumps(self.handledata)
        logger.debug(
            f"Will now modify handle {self.prefix}/{suffix} "
            "so that it will contain {handlejson}"
        )
        self._request(
            put, f"api/handles/{self.prefix}/{suffix}?index=various", data=handlejson
        )

    def delete_session(self):
        """Delete the current session. This is strongly adviced to do and is also called by
        the destructor of this class.

        Raises
        ------
        HandleClientException
            If no session is active, this will be raised.
        """
        logger.debug("Deleting session...")
        if not self.session:
            logger.error("No active session, cannot delete.")
            raise HandleClientException("Cannot delete a session when none is active.")
        delresp = self._request(delete, "api/sessions/this")
        logger.debug(f"Response code {delresp.status_code} returned.")
        if delresp.status_code != 204:
            logger.error(
                f"Response code {delresp.status_code} is not " "the expected one (204)."
            )
            raise HandleClientException(
                f"Unexpected session deletion response code {delresp.status_code}"
            )
        logger.debug("Session deletion success!")
        self.session = None

    def list_my_handles(self):
        """List all handles from the currently managed prefix.

        Returns
        -------
        json
            List of all the handles within the context of the current prefix.
        """
        logger.debug(f"Request session for the authenticated prefix ({self.prefix}).")
        return self._request(get, f"api/handles?prefix={self.prefix}").json()

    def find_handle(self, hdl):
        """Find handle information.

        Parameters
        ----------
        hdl : str
            Handle to query

        Returns
        -------
        json
            Handle information of the requested handle.
        """
        logger.debug(f"Request to get the information for handle {hdl}")
        hdl_data = self._request(
            get, f"https://hdl.handle.net/api/handles/{hdl}", abs=True
        ).json()
        logger.debug(f"Handle data is {hdl_data}")
        return hdl_data

    def __del__(self):
        """Delete any active session before cleaning up the object."""
        logger.debug("Class destructor called. Will delete session if any!")
        if self.session:
            self.delete_session()
