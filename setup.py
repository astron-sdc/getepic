# SPDX-License-Identifier: Apache-2.0
from setuptools import setup, find_packages
import json

with open("codemeta.json") as cm_file:
    cm_data = json.load(cm_file)
    version = cm_data["version"]

setup(
    name='getepic',
    version=version,
    packages=find_packages(where='lib'),
    package_dir={'': 'lib'},
    scripts=['bin/getepic'],
    author='ASTRON',
    description='Tool to request EPIC PIDs and store the result in a database.',
    install_requires=[
        'SQLAlchemy==1.4.24',
        'requests==2.25'
    ],
    license="Apache License, Version 2.0",
    entry_points={
        'console_scripts': ['get_epic=getepic.get_epic:main']
    },
    test_suite="getepic.tests"
)
